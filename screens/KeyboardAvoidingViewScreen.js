import React, { Component } from 'react';
import { Text, Button, TextInput, KeyboardAvoidingView, View, StyleSheet } from 'react-native';

export default class KeyboardAvoidingViewScreen extends Component {

  render() {
    return (
      <View style={styles.container}>
        <KeyboardAvoidingView behavior="padding" enabled>
          <TextInput
            style={styles.input}
            placeholder="Text input"
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="Text input"
            returnKeyType="return"
            // keyboardVerticalOffset={50}
            blurOnSubmit={true}
          />
          
        </KeyboardAvoidingView>
        <Button
            title="Home"
            onPress={() => this.props.navigation.navigate('Home')}
        />
      </View>
    );
  }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
  },
  input: {
    margin: 20,
    marginBottom: 0,
    height: 34,
    paddingHorizontal: 10,
    borderRadius: 4,
    borderColor: '#ccc',
    borderWidth: 1,
    fontSize: 16,
  },
});
